class DivisionByZeroException extends Exception {


    public DivisionByZeroException() {

        super("Error: You cannot divide by zero.");

    }

}