class NegativeNumberException extends Exception {


    public NegativeNumberException() {

        super("Error: Input cannot be negative.");

    }

}