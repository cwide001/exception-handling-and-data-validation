import java.util.InputMismatchException;

import java.util.Scanner;

class ExceptionHandling {

    static Scanner scan = new Scanner(System.in);

    public static double returnRatio() throws DivisionByZeroException,

            InputMismatchException, NegativeNumberException {

        double ratio = 0;

        System.out.println("Enter a number:");

        int num1 = scan.nextInt();

        if (num1 < 0) {

            throw new NegativeNumberException();

        }

        System.out.println("Enter the second number:");

        int num2 = scan.nextInt();

        if (num2 == 0) {


            throw new DivisionByZeroException();

        } else if (num2 < 0) {


            throw new NegativeNumberException();

        }

        ratio = (double) num1 / num2;

        return ratio;

    }

    public static void main(String[] args) {

        boolean done = false;

        while (!done) {

            try {

                double ratio = returnRatio();

                System.out.println("Answer is " + ratio);

                done = true;

            } catch (InputMismatchException e) {

                System.out.println("ERROR: Input should be an integer");


                scan.nextLine();

            } catch (DivisionByZeroException e) {

                System.out.println(e.getMessage());

                scan.nextLine();

            } catch (NegativeNumberException e) {

                System.out.println(e.getMessage());

                scan.nextLine();

            }

        }

    }

}